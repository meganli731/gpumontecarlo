package monteCarloSimulation;

import java.util.ArrayList;

import junit.framework.TestCase;

public class Test_BMStockPath extends TestCase {
	//test constructor
	public void test1() {
		double s0=152.35;
		double r=0.0001;
		double sigma=0.01;
		int period=252;
		BMStockPath bmpath=new BMStockPath(s0, r, sigma, period);
		assertEquals(bmpath.s0, s0);
		assertEquals(bmpath.r, r);
		assertEquals(bmpath.sigma, sigma);
		assertEquals(bmpath.period, period);
	}
	
	//test getPrices method
	public void test2() {
		double s0=152.35;
		double r=0.0001;
		double sigma=0.01;
		int period=252;
		BMStockPath bmpath=new BMStockPath(s0, r, sigma, period);
		assertEquals(bmpath.r, r);
		assertEquals(bmpath.sigma, sigma);
		assertEquals(bmpath.period, period);
		
		ArrayList<DatePricePair> pricesList=bmpath.getPrices();
		ArrayList<DatePricePair> antiTheticPricesList=bmpath.getPrices();

		//This is very likely to hold
		assertTrue((pricesList.get(0).getPrice()-s0)<6);
		
		assertEquals(pricesList.size(), period);
		
		/*for (DatePricePair dp: pricesList) {
			System.out.println(dp.getPrice()+"  ");
		}*/
	}
}
