package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_EuroCallPayout extends TestCase {

	//test constructor
	public void test1() {
		double strick=165; 
		
		EuroCallPayout euPay=new EuroCallPayout(strick);
		assertEquals(euPay.strick, strick);
	}
	
	//test getPayout method
	public void test2() {
		BMStockPath bmpath=new BMStockPath(152.35, 0.0001, 0.01, 252);
		double strick=165.0;
		EuroCallPayout euPay=new EuroCallPayout(strick);
		
		//System.out.println(euPay.getPayout(bmpath));
		//System.out.println(euPay.getPayout(bmpath));
		
		assertTrue(euPay.getPayout(bmpath)>=0);
		assertTrue(euPay.getPayout(bmpath)<150);
	}
}
