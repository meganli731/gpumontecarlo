package monteCarloSimulation;

import junit.framework.TestCase;

//This class test the simulation manager class
public class Test_SimulationManager extends TestCase {
	
	//test euroCallPrice method
	public void test1() {
		//parameters that determines option prices
		double s0=152.35;
		double r=0.0001;
		double sigma=0.01;
		int period=252;
		double strick=165;
		
		//call SimulationManager.asianCallPrice method to price european call option
		double euCall=SimulationManager.euroCallPrice(s0, r, sigma, period, strick);
		
		//The european call option price is as follows
		assertEquals(euCall, 6.22, 0.05);
		System.out.println("European call price is "+euCall);
	}
	
	//test asianCallPrice method
		public void test2() {
			//parameters that determines option prices
			double s0=152.35;
			double r=0.0001;
			double sigma=0.01;
			int period=252;
			double strick=164;
			
			//call SimulationManager.asianCallPrice method to price asian call option
			double asianCall=SimulationManager.asianCallPrice(s0, r, sigma, period, strick);
			
			//The asian call option price is as follows
			assertEquals(asianCall, 2.19, 0.05);
			System.out.println("Asian call price is "+asianCall);
		}
	
}
