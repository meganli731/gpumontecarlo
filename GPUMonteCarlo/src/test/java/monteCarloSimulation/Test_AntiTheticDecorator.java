package monteCarloSimulation;

import java.util.Arrays;

import junit.framework.TestCase;

public class Test_AntiTheticDecorator extends TestCase {
	
	//test constructor
	public void test1() {
		NormalRVG nrvg=new NormalRVG(2);
		AntiTheticDecorator atd=new AntiTheticDecorator(nrvg);
	}
	
	//test getVectot
	public void test2() {
		NormalRVG nrvg = new NormalRVG(252);
		AntiTheticDecorator atd=new AntiTheticDecorator(nrvg);
		
		double[] nrv=Arrays.copyOf(atd.getVector(), 252);
		double[] negativeNrv=atd.getVector();
		
		//System.out.println("Random Vector"+nrv[0]+", "+nrv[1]);
		//System.out.println("Random Vector"+negativeNrv[0]+", "+negativeNrv[1]);
		
		assertTrue(nrv[0]+negativeNrv[0]==0);
		assertTrue(nrv[1]+negativeNrv[1]==0);
		assertTrue(nrv.length==252);
		assertTrue(negativeNrv.length==252);
	}
}
