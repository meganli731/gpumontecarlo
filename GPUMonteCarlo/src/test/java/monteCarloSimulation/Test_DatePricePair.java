package monteCarloSimulation;

import junit.framework.TestCase;

import org.joda.time.DateTime;

public class Test_DatePricePair extends TestCase {

	//test constructor
	public void test1 () {
		DateTime date=new DateTime(2013, 10, 10, 8, 0);
		double price=152.0;
		@SuppressWarnings("unused")
		DatePricePair dpPair=new DatePricePair(date, price);
	}
	
	//test getDate
	public void test2 () {
		DateTime date=new DateTime(2013, 10, 10, 8, 0);
		double price=152.0;
		DatePricePair dpPair=new DatePricePair(date, price);

		assertEquals(dpPair.getDate().year().get(), 2013);
		assertEquals(dpPair.getDate().monthOfYear().get(), 10);
		assertEquals(dpPair.getDate().dayOfMonth().get(), 10);
		assertEquals(dpPair.getDate().hourOfDay().get(), 8);
		assertEquals(dpPair.getDate().minuteOfHour().get(), 0);
	}
	
	//test getPrice
	public void test3 () {
		DateTime date=new DateTime(2013, 10, 10, 8, 0);
		double price=152.0;
		DatePricePair dpPair=new DatePricePair(date, price);
		
		assertEquals(dpPair.getPrice(), price);
	}
}
