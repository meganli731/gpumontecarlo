package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_StdEstimation extends TestCase {
	//test estimateStd method
	public void test1 () {
		EuroCallPayout eucallPayout=new EuroCallPayout(165);
		BMStockPath bmpath=new BMStockPath(152.35, 0.0001, 0.01, 252);
		double stdest = StdEstimator.estimateStd(eucallPayout, bmpath, 100000);
		//System.out.println(stdest);
		assertTrue(stdest>12.5);
		assertTrue(stdest<13.5);
	}
	
	//test estimateStd method
	public void test2 () {
		AsianCallPayout asianCallPayout=new AsianCallPayout(165);
		BMStockPath bmpath=new BMStockPath(152.35, 0.0001, 0.01, 252);
		double stdest = StdEstimator.estimateStd(asianCallPayout, bmpath, 100000);
		//System.out.println(stdest);
		assertTrue(stdest>5.3);
		assertTrue(stdest<5.5);
	}
}
