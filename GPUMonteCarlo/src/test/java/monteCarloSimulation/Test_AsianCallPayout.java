package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_AsianCallPayout extends TestCase {
	//test constructor
		public void test1() {
			double strick=164; 
			
			AsianCallPayout euPay=new AsianCallPayout(strick);
			assertEquals(euPay.strick, strick);
		}
		
		//test getPayout method
		public void test2() {
			BMStockPath bmpath=new BMStockPath(152.35, 0.0001, 0.01, 252);
			double strick=165.0;
			AsianCallPayout euPay=new AsianCallPayout(strick);
			
			//System.out.println(euPay.getPayout(bmpath));
			//System.out.println(euPay.getPayout(bmpath));
			
			assertTrue(euPay.getPayout(bmpath)>=0);
			assertTrue(euPay.getPayout(bmpath)<150);
		}
}
