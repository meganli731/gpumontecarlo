package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_NormalRVG extends TestCase {
	//test Constructor
	public void test1() {
		NormalRVG nrvg = new NormalRVG(2);
		assertEquals(nrvg.dimension, 2);
	}
	
	//test getVector method
	public void test2() {
		NormalRVG nrvg = new NormalRVG(252);
		assertEquals(nrvg.dimension, 252);
		
		double[] nrv=nrvg.getVector();
		
		//System.out.println("Random Vector"+nrv[0]+", "+nrv[1]);
		
		assertTrue(nrv[0] != nrv[1]);
		assertTrue(nrv.length==252);
	}
	
	
}
