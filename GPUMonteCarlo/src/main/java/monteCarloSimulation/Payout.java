package monteCarloSimulation;

public interface Payout {
	public double getPayout(StockPath path);
}
