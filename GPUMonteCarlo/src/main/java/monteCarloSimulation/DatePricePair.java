package monteCarloSimulation;

import org.joda.time.DateTime;

//This class is a pair of dateTime and price
public class DatePricePair {
	protected DateTime date;
	protected double price;
	
	public DatePricePair(DateTime adate, double aprice) {
		date=new DateTime(adate);
		price=aprice;
	}
	
	public DateTime getDate() {
		return new DateTime(date);
	}
	
	public double getPrice() {
		return price;
	}
}
