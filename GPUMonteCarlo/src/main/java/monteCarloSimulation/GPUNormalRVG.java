
package monteCarloSimulation;


import org.bridj.Pointer;
import static org.bridj.Pointer.allocateFloats;
import com.nativelibs4java.opencl.*;
import com.nativelibs4java.opencl.JavaCL;


public class GPUNormalRVG implements RandomVectorGenerator{

	
	    // length of random vector 
	    private int N;
	    // batch number 
	    private int batchNum;
	    // store the normal random vector stored in GPU to generate Gaussian
	    private double[] normals;
	    private int index;

	    /*
	    *GPU Generator Constructor
	    * */
	    public GPUNormalRVG(int N, int batchNum){
	        this.N = N;
	        this.batchNum = batchNum;
	        normals = getGPUGaussian(this.batchNum);
	        index = 0;
	    }
	    /*
	    * This function aims to generate the normal random variable with length N and use the GPU to compute 2 million numbers
	    * */
	    public double[] getVector() {
	        double[] vector = new double[N];
	        for(int i = 0; i < N; i++){
	            if(index == 2*batchNum - 1) {
	                normals = getGPUGaussian(this.batchNum);
	            }
	            vector[i] = normals[index];
	            index++;
	        }
	        return vector;
	    }

	    /*
	    * Generate normal random variable using the best device chosen by kernel
	    * */
	    public double[] getGPUGaussian(int batchNum){

	        index = 0;

	        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
	        CLDevice device = clPlatform.listAllDevices(true)[0];
	        CLContext context = JavaCL.createContext(null, device);
	        //  make a default FIFO queue.
	        CLQueue queue = context.createDefaultQueue();
	       // Read the program sources and compile them :
	        String src =
					"__kernel void fill_in_values(global const float* a, global const float* b, global float* out1, global float* out2, float pi, int n) \n" +
	                "{\n" +
	                "    int i = get global id(0);\n" +
	                "    if (i >= n)\n" +
	                "        return;\n" +
	                "\n" +
	                "    out1[i] = sqrt(-2*log(a[i]))*cos(2*pi*b[i]);\n" +
	                "    out2[i] = sqrt(-2*log(a[i]))*sin(2*pi*b[i]);\n" +
	                "}";
	        CLProgram program = context.createProgram(src);
	        program.build();
	        CLKernel kernel = program.createKernel("please fill in values");
	        final int n = batchNum;
	        final Pointer<Float>
	                aPtr = allocateFloats(n),
	                bPtr = allocateFloats(n);
	        
	        double[] uniforms = (new UniformRandomNumberGenerator(2*batchNum)).getVector();
	        // Generate uniform sequence and assign to aPtr and bPtr
	        for (int i = 0; i < n; i++) {
	            aPtr.set(i, (float) uniforms[2*i]);
	            bPtr.set(i, (float) uniforms[2*i+1]);
	        }

	        // Create OpenCL input buffers with native memory pointers 
	        CLBuffer<Float>
	                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
	                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);
	        CLBuffer<Float>
	                out1 = context.createFloatBuffer(CLMem.Usage.Output, n),
	                out2 = context.createFloatBuffer(CLMem.Usage.Output, n);
	        kernel.setArgs(a, b, out1, out2, (float) Math.PI, batchNum);
	        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});
	        event.invokeUponCompletion(new Runnable() {
	            public void run() {}
	        });
	        final Pointer<Float> c1Ptr = out1.read(queue,event);
	        final Pointer<Float> c2Ptr = out2.read(queue,event);

	        double[] normals = new double[2*batchNum];
	        for(int i = 0; i < batchNum; i++){
	            normals[2*i] = (double) c1Ptr.get(i);
	            normals[2*i+1] = (double) c2Ptr.get(i);
	        }
	        return normals;
	    }

	}

