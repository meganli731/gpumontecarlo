package monteCarloSimulation;

import java.util.ArrayList;
import org.joda.time.DateTime;

//generate a brownian motion stock path
public class BMStockPath implements StockPath{
	
	protected double s0;
	protected double r;
	protected double sigma;
	protected int period;
	protected AntiTheticDecorator atd;
	
	//get parameters of the path in constructor
	public BMStockPath(double as0, double ar, double asigma, int aperiod) {
		s0=as0;
		r=ar;
		sigma=asigma;
		period=aperiod;
		atd=new AntiTheticDecorator(new NormalRVG(period));
	}
	
	//return a list of DateTime prices pairs. The prices follows geometric brownian motion
	public ArrayList<DatePricePair> getPrices() {
		//store stock path in the ArrayList
		ArrayList<DatePricePair> pricesList=new ArrayList<DatePricePair>(period+1);
		
		DateTime date=new DateTime(2013, 10, 10, 8, 0);
		double price=s0;
		double[] nrv=atd.getVector();
		
		double t=(r-Math.pow(sigma, 2)/2);	//compute here to speed up the program a little bit
		for (int i=0; i<period; i++) {
			date=date.plusDays(1);
			double power=t+sigma*nrv[i];
			price=price*Math.exp(power);
			pricesList.add(new DatePricePair(date, price));
		}
		return pricesList;
	}
}
