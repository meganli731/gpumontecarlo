package monteCarloSimulation;

//The class is used to estimate standard deviation of option payout
public class StdEstimator {
	
	/*
	 * The method is used to estimate standard deviation of option payout. 
	 * Parameter n is the number of simulation to run
	 */
	public static double estimateStd(Payout payout, StockPath spath, int n) {
		double mean=0;
		double xSqr=0;
		for (int i=0; i<n; i++) {
			double x=payout.getPayout(spath);
			mean+=x;
			xSqr+=Math.pow(x, 2);
		}
		return Math.pow((xSqr/n-Math.pow(mean/n, 2)), 0.5);
	}
}
