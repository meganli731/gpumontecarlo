package monteCarloSimulation;

import java.util.ArrayList;

public interface StockPath {
	public ArrayList<DatePricePair> getPrices();
}
