package monteCarloSimulation;


//This class generates anti-thetic random vectors
public class AntiTheticDecorator implements RandomVectorGenerator{
	protected RandomVectorGenerator rvg;
	protected double[] vector;
	protected boolean firstGet;
	
	//construct the decorator with a random number generator
	public AntiTheticDecorator (RandomVectorGenerator arvg) {
		rvg=arvg;
		firstGet=true;
	}
	
	//this method returns a vector and negation of all component in the next call
	public double[] getVector() {
		if (firstGet==true) {
			vector=rvg.getVector();
			firstGet=false;
			return vector;
		} else {
			firstGet=true;
			for (int i=0; i<vector.length; i++) {
				vector[i]=-vector[i];
			}
			return vector;
		}		
	}
}
