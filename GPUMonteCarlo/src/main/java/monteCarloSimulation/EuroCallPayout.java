package monteCarloSimulation;

import java.util.ArrayList;

//This class is used to compute european call option payout given a stock path
public class EuroCallPayout implements Payout {
	
	protected double strick;
	
	//construct an object with strick price
	public EuroCallPayout(double astrick) {
		strick=astrick;
	}
	
	//calcualte payout given a stock path
	public double getPayout (StockPath path) {
		ArrayList<DatePricePair> pricesList=path.getPrices();
		
		//System.out.println(pricesList.get(pricesList.size()-1).getPrice());
		
		return Math.max(0, pricesList.get(pricesList.size()-1).getPrice()-strick);
	}
}
