package monteCarloSimulation;

import java.util.ArrayList;

//This class calculate asian call option payout
public class AsianCallPayout implements Payout {
	
	protected double strick;
	
	//construct an object with a strick price
	public AsianCallPayout(double astrick) {
		strick=astrick;
	}
	
	//calculate payout from a stock path
	public double getPayout (StockPath path) {
		ArrayList<DatePricePair> pricesList=path.getPrices();
		double average=0;
		for (DatePricePair dp: pricesList) {
			average+=dp.getPrice();
		}
		
		//System.out.println(pricesList.get(pricesList.size()-1).getPrice());
		
		return Math.max(0, average/pricesList.size()-strick);
	}
}
