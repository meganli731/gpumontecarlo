package monteCarloSimulation;

import java.util.Random;


//This class generate a normal random vector
public class NormalRVG implements RandomVectorGenerator {

	protected int dimension;
	
	//n is how many component you want to have in a random vector
	public NormalRVG (int n) {
		dimension=n;
	}
	
	//The method returns a vector of standard normal random vector
	public double[] getVector() {
		double[] randomVector=new double[dimension];
		Random random=new Random();
		for (int i=0; i<dimension; i++) {
			randomVector[i]=random.nextGaussian();
		}
		return randomVector;
	}
}
