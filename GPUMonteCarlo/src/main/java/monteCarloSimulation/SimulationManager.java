package monteCarloSimulation;

//This class has two methods to price european call option and asian call option
public class SimulationManager {
	
	//The method to price european call option
	public static double euroCallPrice(double s0, double r, double sigma, int period, double strick) {
		EuroCallPayout payout=new EuroCallPayout(strick);
		BMStockPath spath=new BMStockPath(s0, r, sigma, period);
		int n=100000;
		
		double stdest=StdEstimator.estimateStd(payout, spath, n);
		System.out.println("The estimated standard deviation is "+stdest);
		int numOfSim=(int) Math.pow(2.055*stdest/0.01, 2);
		double meanPayout=0;
		System.out.println("# of simulation to price european call option: "+numOfSim);
		for (int i=0; i<numOfSim; i++) {
			//if (i%500000==0)
				//System.out.println("In the "+i+"'s simulation");
			meanPayout+=payout.getPayout(spath);
		}
		return (meanPayout/numOfSim)*Math.exp(-r*period);
	}
	
	//The method to price asian call option
	public static double asianCallPrice(double s0, double r, double sigma, int period, double strick) {
		AsianCallPayout payout=new AsianCallPayout(strick);
		BMStockPath spath=new BMStockPath(s0, r, sigma, period);
		int n=100000;
		
		double stdest=StdEstimator.estimateStd(payout, spath, n);
		System.out.println("The estimated standard deviation is "+stdest);
		int numOfSim=(int) Math.pow(2.055*stdest/0.01, 2);
		double meanPayout=0;
		System.out.println("# of simulation to price asian call option: "+numOfSim);
		for (int i=0; i<numOfSim; i++) {
			//if (i%100000==0)
				//System.out.println("In the "+i+"'s simulation");
			meanPayout+=payout.getPayout(spath);
		}
		return (meanPayout/numOfSim)*Math.exp(-r*period);
	}
	
	
}
