Readme file 

Monte Carlo Simulation assignment
Shimeng Li
N16479448

Before you can run the code, you should add Joda-time library to you build path. Adding a library/.jar file is easy in an IDE. 

Because the program has random component, if the result is not as expected in the first run, please run the program for a second time.

The running time of the program is about 5 minutes.

Please start looking at the code from Test_Simulation manager class. The class has two tests. One tests European call option price, the other tests Asian call option price. 

The output of the test case should look like:

1.The estimated standard deviation is 13.016083445943897
2.# of simulation to price european call option: 7154582
3.European call price is 6.2147233313571135
4.The estimated standard deviation is 5.727430454160297
5.# of simulation to price asian call option: 1385298
6.Asian call price is 2.204054221593144

The 1st line is the estimated standard deviation of European call option's payout.
The 2nd line is the number of simulation needed to get the required accuracy.
The 3rd line is the European option's price.
The 4th line is the estimated standard deviation of Asian call option's payout.
The 5th line is the number of simulation needed to get the required accuracy.
The 6th line is the Asian option's price.
